let book_category = {
    bootstrap: false,
    init: function () {
        if (this.bootstrap) {
            return false;
        }
        this.bootstrap = true;

        $(document).on('submit', '#add-book-category', function (e) {
            e.preventDefault();
            book_category.submitAddBookCategory();
        });
    },

    submitAddBookCategory: function () {
        let form_data = new FormData($('#add-book-category')[0]);
        $.ajax({
            url: '/category/store',
            type: 'POST',
            dataType: JSON,
            processData: false,
            contentType: false,
            success: function (data) {
                if(data.status) {
                    window.location.reload();
                } else {
                    $('.ajax-message').html(data.msg);
                    $('#addTeam').modal('hide');
                }
            },
            error: function (data) {
                $('.message-validate').html('');
                $.each(data.responseJSON.errors, function (key, value) {
                    $('#addTeamForm span[data-error="' + key + '"]').html(value);
                });
            }
        });
    },
};

$(document).ready(function () {
    book_category.init();
});