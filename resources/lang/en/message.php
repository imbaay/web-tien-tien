<?php

return [
    'book_category' => [
        'add' => [
            'success' => 'Add new category successed.',
            'fail' => 'Add new category failed.',
        ],
    ],
];