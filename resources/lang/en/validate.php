<?php

return [
    'book_category' => [
        'book_category_name' => [
            'required' => 'The :attribute is required',
        ],
    ],
];