@extends('backend.layout.index')
@section('title')
    Book Online
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong>Search</strong> Book
                </div>
                <div class="card-body card-block">
                    <form action="" method="get" class="form-horizontal" role="search">
                        <div class="row form-group">
                            <div class="col col-md-2">
                                <label class=" form-control-label">Book name </label>
                            </div>
                            <div class="col-12 col-md-10">
                                <input class="form-control" type="search" name="book_name">
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary btn-sm m10r">
                                Search
                            </button>
                            <a href="">
                                <button type="reset" class="btn btn-secondary btn-sm">
                                <span class="character-reset">
                                    Reset
                                </span>
                                </button>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @include('backend.partial.paginate', ['data' => $books])
        <div class="col-md-12">
            <div class=" col-md-12 bd rounded table-responsive">
                <table class="table table-hover mg-b-0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Author</th>
                            <th>Price</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if($books->isNotEmpty())
                        @foreach($books as $book)
                            <tr>
                                <td>{{ $book->id }}</td>
                                <td>{{ $book->book_name }}</td>
                                <td>{{ $book->book_categories->category_name }}</td>
                                <td>{{ $book->author }}</td>
                                <td>{{ $book->price }}</td>
                                <td></td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
        @include('backend.partial.paginate', ['data' => $books])
    </div>
@endsection
