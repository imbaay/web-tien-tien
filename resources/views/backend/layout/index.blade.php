<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Online Book Website.">
    <meta name="author" content="tien.bm">
    <meta name="keyword" content="tien.bm">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    <!-- CSS -->
    <link href="{{ asset('backend/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/ionicons.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/perfect-scrollbar.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/jquery.switchButton.css') }}" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="{{ asset('backend/css/bracket.css') }}">
</head>
<body>
<!-- LOGO -->
<div class="br-logo"><a href="javascript:void(0)"><span>[</span>Manh Tien<span>]</span></a></div>
<!-- END LOGO -->

<!-- START SIDEBAR -->
@include('backend.layout.sidebar')
<!-- END SIDEBAR -->

<!-- START HEADER -->
@include('backend.layout.header')
<!-- START HEADER -->

<!-- START: MAIN CONTENT -->
<div class="br-mainpanel">
    <!-- START Breadcrumb -->
    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="javascript:void(0)">Home</a>
        </nav>
    </div>
    <!-- END Breadcrumb -->

    <div class="br-pagebody">
        @yield('content')
    </div>

    <footer class="br-footer">
        <div class="footer-left">
            <div class="mg-b-2">Copyright &copy; 2019. Manh Tien. </div>
            <div>All Rights Reserved.</div>
        </div>
        <div class="footer-right d-flex align-items-center">
            <span class="tx-uppercase mg-r-10">Share:</span>
            <a target="_blank" class="pd-x-5" href="https://www.facebook.com/sharer/sharer.php?u=http%3A//themepixels.me/bracket/intro"><i class="fa fa-facebook tx-20"></i></a>
            <a target="_blank" class="pd-x-5" href="https://twitter.com/home?status=Bracket,%20your%20best%20choice%20for%20premium%20quality%20admin%20template%20from%20Bootstrap.%20Get%20it%20now%20at%20http%3A//themepixels.me/bracket/intro"><i class="fa fa-twitter tx-20"></i></a>
        </div>
    </footer>
</div>
<!-- END: MAIN CONTENT -->

<script src="{{ asset('backend/js/jquery.js') }}"></script>
<script src="{{ asset('backend/js/popper.js') }}"></script>
<script src="{{ asset('backend/js/bootstrap.js') }}"></script>
<script src="{{ asset('backend/js/perfect-scrollbar.jquery.js') }}"></script>
<script src="{{ asset('backend/js/moment.js') }}"></script>
<script src="{{ asset('backend/js/jquery-ui.js') }}"></script>
<script src="{{ asset('backend/js/jquery.switchButton.js') }}"></script>
<script src="{{ asset('backend/js/jquery.peity.js') }}"></script>

<script src="{{ asset('backend/js/bracket.js') }}"></script>
<script src="{{ asset('backend/js/ResizeSensor.js') }}"></script>
</body>
