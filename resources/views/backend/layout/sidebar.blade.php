<!-- START: SIDEBAR -->
<?php
$home = ['/'];
$book = ['/book'];
$book_categories = ['/category']
?>

<div class="br-sideleft overflow-y-auto">
    <label class="sidebar-label pd-x-15 mg-t-20">Navigation</label>
    <div class="br-sideleft-menu">
        <a href="/" class="br-menu-link {{ activeRoute($home) }}">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
                <span class="menu-item-label">Home</span>
            </div>
        </a>
        <a href="{{ route(BOOK_LIST) }}" class="br-menu-link {{ activeRoute($book) }}">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-ios-book-outline tx-22"></i>
                <span class="menu-item-label">Book</span>
            </div>
        </a>
        <a href="{{ route(CATEGORY_LIST) }}" class="br-menu-link {{ activeRoute($book_categories) }}">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-ios-book-outline tx-22"></i>
                <span class="menu-item-label">Category</span>
            </div>
        </a>
        <a href="javascript:void(0)" class="br-menu-link">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-ios-person-outline tx-22"></i>
                <span class="menu-item-label">User</span>
            </div>
        </a>
    </div>
</div>
<!-- END: SIDEBAR -->
