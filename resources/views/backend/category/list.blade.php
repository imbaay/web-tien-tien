@extends('backend.layout.index')
@section('title')
    Book Categories
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong>Search</strong> Book Categoeies
                </div>
                <div class="card-body card-block">
                    <form method="GET" class="form-horizontal" role="search">
                        <div class="row form-group">
                            <div class="col col-md-2">
                                <label class=" form-control-label">Category name </label>
                            </div>
                            <div class="col-12 col-md-10">
                                <input class="form-control" type="search" name="category_name">
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary btn-sm m10r">
                                Search
                            </button>
                            <a href="">
                                <button type="reset" class="btn btn-secondary btn-sm">
                                <span class="character-reset">
                                    Reset
                                </span>
                                </button>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12 mg-t-15">
            <button class="btn btn-purple" data-toggle="modal" data-target="#add-category"><i class="fa fa-plus mg-r-10"></i> ADD CATEGORY</button>
        </div>
        @include('backend.partial.paginate', ['data' => $book_categories])
        <div class="col-md-12">
            <div class=" col-md-12 bd rounded table-responsive">
                <table class="table table-hover mg-b-0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Category name</th>
                            <th>Book</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if($book_categories->isNotEmpty())
                        @foreach($book_categories as $book_category)
                            <tr>
                                <td>{{ $book_category->id }}</td>
                                <td>{{ $book_category->category_name }}</td>
                                <td>{{ $book_category->books_count }}</td>
                                <td></td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
        @include('backend.partial.paginate', ['data' => $book_categories])
    </div>
    @include('backend.category.add')
@endsection