<div id="add-category" class="modal fade">
    <div class="modal-dialog modal-dialog-vertical-center" role="document">
        <div class="modal-content bd-0 tx-14">
            <form method="POST" action="{{ route(CATEGORY_STORE) }}">
                @csrf
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Add Category</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pd-25">
                    <label for="book_category"> Category Name <span>*</span></label>
                    <input type="text" class="form-control" name="book_category_name" />
                    @if(!empty($errors->has('book_category_name')))
                        <span class="message-validate">{{ $errors->first('book_category_name') }}</span>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="reset" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div><!-- modal-dialog -->
</div>