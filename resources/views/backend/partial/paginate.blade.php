<div class="col-md-12 mg-t-15">
@if($data->isNotEmpty())
    <div class="float-md-left">
        {{ $data->firstItem() . ' 〜 ' . $data->lastItem() . '（' . $data->total() . '）' }}
    </div>
@endif
<ul class="pagination float-right">
    {{ $data->appends(request()->all())->links() }}
</ul>
</div>
