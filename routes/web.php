<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('backend.layout.index');
});

Route::group(['prefix' => 'category'], function () {
    Route::get('/', 'BookCategoriesController@index')->name(CATEGORY_LIST);
    Route::post('store', 'BookCategoriesController@store')->name(CATEGORY_STORE);
});

Route::group(['prefix' => 'book'], function () {
    Route::get('/', 'BooksController@index')->name(BOOK_LIST);
});
