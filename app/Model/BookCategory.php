<?php

namespace App\Model;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class BookCategory extends Model
{
    use SoftDeletes;

    protected $table = 'book_categories';
    protected $fillable = ['category_name'];
    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    public function books()
    {
        return $this->hasMany(Book::class, 'category_id');
    }

    public static function getListCategoryDropdownList()
    {
        return BookCategory::select('id', 'category_name')
            ->pluck('category_name', 'id')
            ->toArray();
    }

    public static function getAllBookCategories($params = [])
    {
        $conditions = [];

        if (!empty($params['category_name'])) {
            array_push($conditions, ['category_name', 'like', '%' . $params['category_name'] . '%']);
        }

        return BookCategory::select('id', 'category_name')
            ->where($conditions)
            ->withCount('books')
            ->orderBy('id', 'desc')
            ->paginate('10');
    }

    public static function addCategory($input)
    {
        try {
            BookCategory::create($input);
            return true;
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            return false;
        }
    }
}
