<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    use SoftDeletes;

    protected $table = 'books';
    protected $fillable = [
        'book_name',
        'category_id',
        'author',
        'price'
    ];
    protected $dates = [
        'deleted_at',
        'created_at',
        'update_at'
    ];

    public function book_categories()
    {
        return  $this->belongsTo(BookCategory::class, 'category_id');
    }

    public static function getAllBooks($params = [])
    {
        $conditions = [];

        if (!empty($params['book_name'])) {
            array_push($conditions, ['book_name', 'like', '%' . $params['book_name'] . '%']);
        }

        return Book::select('id', 'book_name', 'category_id', 'author', 'price')
            ->where($conditions)
            ->orderBy('id', 'desc')
            ->paginate('10');
    }
}
