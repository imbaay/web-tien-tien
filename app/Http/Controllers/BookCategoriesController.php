<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookCategoryRequest;
use App\Model\BookCategory;

class BookCategoriesController extends Controller
{
    public function index()
    {
        $params = request()->all();
        $book_categories = BookCategory::getAllBookCategories($params);
        return view('backend.category.list', compact('book_categories'));
    }

    public function store(BookCategoryRequest $request)
    {
        $input = $request->all();
        $status = false;
        $message = '';
        if($request->ajax()) {
            if(BookCategory::addCategory($input)) {
                session()->flash(SUCCESS, __('message.book_category.add.success'));
                $status = true;
            } else {
                $message = __('message.book_category.add.fail');
            }

            return response()->json(['status' => $status, 'message' => $message]);
        }
    }
}
