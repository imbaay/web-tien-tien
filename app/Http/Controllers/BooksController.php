<?php

namespace App\Http\Controllers;

use App\Model\Book;
use App\Model\BookCategory;
use Illuminate\Http\Request;

class BooksController extends Controller
{
    public function index()
    {
        $params = request()->all();
        $books = Book::getAllBooks($params);
        $book_categories = BookCategory::getListCategoryDropdownList();
        return view('backend.book.list', compact('books', 'book_categories'));
    }
}
