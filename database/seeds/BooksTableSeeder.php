<?php

use Illuminate\Database\Seeder;
use App\Model\Book;
use App\Model\BookCategory;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Book::truncate();
        $faker = \Faker\Factory::create();
        $categoryId = BookCategory::select('id')->pluck('id', 'id')->toArray();

        for ($i = 0; $i < 20; $i++) {
            Book::create(
                [
                    'book_name' => $faker->name,
                    'category_id' => array_rand($categoryId),
                    'author' => $faker->name,
                    'price' => $faker->year,
                ]
            );
        }
    }
}
