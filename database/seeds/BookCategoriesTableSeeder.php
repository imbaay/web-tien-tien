<?php

use Illuminate\Database\Seeder;
use App\Model\BookCategory;

class BookCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BookCategory::truncate();
        $faker = \Faker\Factory::create();
        for ($i = 0; $i < 20; $i++) {
            BookCategory::create(
                [
                    'category_name' => $faker->name,
                ]
            );
        }
    }
}
